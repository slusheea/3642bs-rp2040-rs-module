# 3642BS driver for embedded devices
![banner](images/banner.gif)

### Makes displaying stuff on the 3642BS easy.
#### [Documentation](https://docs.rs/sevseg_3642bs/latest/sevseg_3642bs/)

Allows you to display dec and hex numbers as well as the time and your own custom characters with a single line of code.
```rs
display.clear().unwrap();
display.number(1234).unwrap();
display.hex(0xF3E2).unwrap();
display.time(10, 30).unwrap();				    //   ===A===
                                                            //  ‖       ‖
// Make your own custom characters:                         //  F       B
let letter_c = SEG_A | SEG_F | SEG_E | SEG_D;               //  ‖       ‖
let letter_o = SEG_G | SEG_E | SEG_C | SEG_D;               //   ===G===
let letter_l = SEG_F | SEG_E | SEG_D;                       //  ‖       ‖
let text_cool = [letter_c, letter_o, letter_o, letter_l];   //  E       C
                                                            //  ‖       ‖
display.custom(text_cool).unwrap();                         //   ===D===
```

## How to use
Include the library on your `cargo.toml`
```toml
sevseg_3642bs = "0.3.2"
```

Define which pins are going to be used:
```rs
let mut display = Display::new(
    pins.gpio16.into_push_pull_output(), // Segment A
    pins.gpio17.into_push_pull_output(), // Segment B
    pins.gpio18.into_push_pull_output(), // Segment C
    pins.gpio19.into_push_pull_output(), // Segment D
    pins.gpio20.into_push_pull_output(), // Segment E
    pins.gpio21.into_push_pull_output(), // Segment F
    pins.gpio22.into_push_pull_output(), // Segment G
    pins.gpio26.into_push_pull_output(), // Two dots
    pins.gpio15.into_push_pull_output(), // Digit 1
    pins.gpio14.into_push_pull_output(), // Digit 2
    pins.gpio13.into_push_pull_output(), // Digit 3
    pins.gpio12.into_push_pull_output(), // Digit 4
    delay, 				 // Your HAL's delay object
);
```

Since the display has to be multiplexed, delays can't be used on the same core the library is on, or the display will freeze on a single digit. For this reason I recommend using alarms or timers. You can also use async tasks with Embassy.

Example using [Alarm0](https://docs.rs/rp2040-hal/latest/rp2040_hal/timer/struct.Alarm1.html) (On the rpi pico):
```rs
// Other code like defining the pins and importing libraries.
// You'll need to import fugit::ExtU32

let mut timer = bsp::hal::Timer::new(pac.TIMER, &mut pac.RESETS);

// Other code like defining the Display struct

let alarm = timer.alarm_1().unwrap();

loop {
    // Other code like calculating "value"

    let _ = alarm.schedule(100_000.micros());

    while !alarm.finished() {
        display.number(value).unwrap();
    }   
}
```

Example using [Timer](https://docs.rs/rp2040-hal/latest/rp2040_hal/timer/struct.Timer.html) (on the rpi pico):
```rs
// Other code like defining the pins and importing libraries

let mut timer = bsp::hal::Timer::new(pac.TIMER, &mut pac.RESETS);
let mut count_down = timer.count_down();

// Other code like defining the Display struct

let mut prev_time = timer.get_counter();
    
loop {
    // Other code like calculating "value"

    while timer.get_counter() < prev_time + 100_000 {
        display.number(value);
    }
    prev_time = timer.get_counter();
}
```

Example using the [Real Time Clock](https://docs.rs/rp2040-hal/latest/rp2040_hal/rtc/index.html) (on the pi pico):
```rs
let mut watchdog = Watchdog::new(pac.WATCHDOG);

let clocks = clocks::init_clocks_and_plls(
    XTAL_FREQ_HZ,
    pac.XOSC,
    pac.CLOCKS,
    pac.PLL_SYS,
    pac.PLL_USB,
    &mut pac.RESETS,
    &mut watchdog,
).ok().unwrap();

let initial_date = DateTime {
    year: 2023,
    month: 1,
    day: 27,
    day_of_week: DayOfWeek::Friday,
    hour: 18,
    minute: 46,
    second: 0,
};

let real_time_clock = RealTimeClock::new(pac.RTC, clocks.rtc_clock, &mut pac.RESETS, initial_date).unwrap();

let mut prev_time = real_time_clock.now().unwrap().second;

loop {
    let mut current_time = real_time_clock.now().unwrap().second;
    while current_time == prev_time {
        display
            .time(
                real_time_clock.now().unwrap().minute,
                current_time,
                current_time,
            )
            .unwrap();
        current_time = real_time_clock.now().unwrap().second;
    }
    prev_time = current_time;
}
```
