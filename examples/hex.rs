// Example for the Raspberry Pi Pico. Other HALs will probably implement things differently, and
// other devices might have different hardware, so this is just orientative.
//
// The crate should be compatible with any device who's HAL implements OutputPin from embedded_hal

#![no_std]
#![no_main]

// Raspberry pi pico stuff
use bsp::entry;
use defmt_rtt as _;
use panic_probe as _;
use rp_pico as bsp;
use bsp::hal::{pac, sio::Sio, timer::Alarm};
const XTAL_FREQ_HZ: u32 = 12_000_000u32;

// Adds .micros(), used to schedule the alarm
use fugit::ExtU32;

use sevseg_3642bs::Display;

#[entry]
fn main() -> ! {
    // Raspberry pi pico stuff
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();
    let sio = Sio::new(pac.SIO);

    // Create the pins struct. Some pins will be needed for the display, the rest are free to use.
    let pins = bsp::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // Create the delay struct required by the display.
    // The display struct will take ownership of the delay struct, since bloacking delays
    // shouldn't be used while using the display.
    let delay = cortex_m::delay::Delay::new(core.SYST, XTAL_FREQ_HZ);

    // Create the timer struct, will provide the hardware alarm later.
    let mut timer = bsp::hal::Timer::new(pac.TIMER, &mut pac.RESETS);
    
    // Define which pins you used for the 7 segment display. Any GPIO pins can be used.
    // Also pass the delay struct.
    let mut display = Display::new(
        pins.gpio16.into_push_pull_output(),    // A segment
        pins.gpio17.into_push_pull_output(),    // B segment
        pins.gpio18.into_push_pull_output(),    // C sengent
        pins.gpio19.into_push_pull_output(),    // D segment
        pins.gpio20.into_push_pull_output(),    // E segment
        pins.gpio21.into_push_pull_output(),    // F segment
        pins.gpio22.into_push_pull_output(),    // G segment
        pins.gpio26.into_push_pull_output(),    // Time dots segment
        pins.gpio15.into_push_pull_output(),    // First common anode
        pins.gpio14.into_push_pull_output(),    // Second common anode
        pins.gpio13.into_push_pull_output(),    // Third common anode
        pins.gpio12.into_push_pull_output(),    // Fourth common anode
        delay,
    );

    // Clear the display for good measure. This isn't really necessary though.
    display.clear().unwrap();
    
    // Initialize the hardware alarm. 
    let mut alarm2 = timer.alarm_2().unwrap();

    loop {
        let display_me = 0xA1B2;
        /*
         * Any code can go here to get the value that will be displayed.
         */

        // Schedule the hardware alarm with the time we want the value to be displayed for/
        let _ = alarm2.schedule(10000.micros());
            
        // Display the number for as long the alarm was scheduled to run.
        while !alarm2.finished() {
            // You can use any of the other abstractions too.
            display.hex(display_me).unwrap();
        }

        /*
         * Any extra code can go here
         */
    }
}
