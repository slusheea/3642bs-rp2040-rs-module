// Example for the Raspberry Pi Pico using Embassy. Other Embassy HALs might look a bit different,
// and other microcontrollers might not have an RTC, so this is just orientative.
//
// The crate should be compatible with any embassy HAL.

#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use embassy_executor::Spawner;
use embassy_time::Delay;
use {defmt_rtt as _, panic_probe as _};

// Raspberry Pi Pico specific
use embassy_rp::{
    gpio::{Level, Output},
    rtc::{DayOfWeek, DateTime, RealTimeClock},
};

use sevseg_3642bs::Display;

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
    // Raspberry Pi Pico specific peripheral definition
    let p = embassy_rp::init(Default::default());
    
    // Set the initial date for the real time clock
    let initial_date = DateTime {
        year: 0,
        month: 0,
        day: 0,
        day_of_week: DayOfWeek::Monday,
        hour: 0,
        minute: 0,
        second: 0,
    };

    // Initialize the real time clock
    let rtc = RealTimeClock::new(p.RTC, initial_date).unwrap();


    // Define which pins you used for the 7 segment display. Any GPIO pins can be used.
    // Also pass the delay struct.
    let mut display = Display::new(
        Output::new(p.PIN_16, Level::High),
        Output::new(p.PIN_17, Level::High),
        Output::new(p.PIN_18, Level::High),
        Output::new(p.PIN_11, Level::High),
        Output::new(p.PIN_20, Level::High),
        Output::new(p.PIN_21, Level::High),
        Output::new(p.PIN_22, Level::High),
        Output::new(p.PIN_26, Level::High),
        Output::new(p.PIN_15, Level::Low),
        Output::new(p.PIN_14, Level::Low),
        Output::new(p.PIN_13, Level::Low),
        Output::new(p.PIN_12, Level::Low),
        Delay,
    );

    loop {
        display.time(rtc.now().unwrap().minute, rtc.now().unwrap().second, rtc.now().unwrap().second).unwrap();
    }
}
