// Example for the Raspberry Pi Pico. Other HALs will probably implement things differently, and
// other devices might have different hardware, so this is just orientative.
//
// The crate should be compatible with any device who's HAL implements OutputPin from embedded_hal

#![no_std]
#![no_main]

// Raspberry pi pico stuff
use bsp::entry;
use defmt_rtt as _;
use panic_probe as _;
use rp_pico as bsp;
use bsp::hal::{
    clocks, pac,
    rtc::{DateTime, DayOfWeek, RealTimeClock},
    sio::Sio,
    Watchdog,
};
const XTAL_FREQ_HZ: u32 = 12_000_000u32;

use sevseg_3642bs::Display;

#[entry]
fn main() -> ! {
    // Raspberry pi pico stuff
    let mut pac = pac::Peripherals::take().unwrap();
    let sio = Sio::new(pac.SIO);
    let core = pac::CorePeripherals::take().unwrap();
    let mut watchdog = Watchdog::new(pac.WATCHDOG);
    let clocks = clocks::init_clocks_and_plls(
        XTAL_FREQ_HZ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    // Create the pins struct. Some pins will be needed for the display, the rest are free to use.
    let pins = bsp::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // Create the delay struct required by the display.
    // The display struct will take ownership of the delay struct, since bloacking delays
    // shouldn't be used while using the display.
    let delay = cortex_m::delay::Delay::new(core.SYST, XTAL_FREQ_HZ);

    // Define which pins you used for the 7 segment display. Any GPIO pins can be used.
    // Also pass the delay struct.
    let mut display = Display::new(
        pins.gpio16.into_push_pull_output(),    // A segment
        pins.gpio17.into_push_pull_output(),    // B segment
        pins.gpio18.into_push_pull_output(),    // C sengent
        pins.gpio19.into_push_pull_output(),    // D segment
        pins.gpio20.into_push_pull_output(),    // E segment
        pins.gpio21.into_push_pull_output(),    // F segment
        pins.gpio22.into_push_pull_output(),    // G segment
        pins.gpio26.into_push_pull_output(),    // Time dots segment
        pins.gpio15.into_push_pull_output(),    // First common anode
        pins.gpio14.into_push_pull_output(),    // Second common anode
        pins.gpio13.into_push_pull_output(),    // Third common anode
        pins.gpio12.into_push_pull_output(),    // Fourth common anode
        delay,
    );

    // Clear the display for good measure. This isn't really necessary though.
    display.clear().unwrap();
    
    // Set an initial date for the real time clock
    let initial_date = DateTime {
        year: 0,
        month: 0,
        day: 0,
        day_of_week: DayOfWeek::Monday,
        hour: 0,
        minute: 0,
        second: 0,
    };
    
    // Initialize the real time clock
    let real_time_clock =
        RealTimeClock::new(pac.RTC, clocks.rtc_clock, &mut pac.RESETS, initial_date).unwrap();
    
    // Initialize previous and current time variable. Is used to detect if the display should update or not.
    let mut prev_time = real_time_clock.now().unwrap().second;
    let mut current_time = real_time_clock.now().unwrap().second;

    loop {
        // If the time hasn't changed, display the time value.
        while current_time == prev_time {
            display
                .time(
                    real_time_clock.now().unwrap().minute,
                    current_time,
                    // The last value always has to be the seconds value, even if it has been
                    // passed previously. This is for the two dots to blink every second.
                    current_time,
                )
                .unwrap();
            current_time = real_time_clock.now().unwrap().second;
        }

        // When the time changes, set the previous time to the current time and start over.
        prev_time = current_time;
    }
}
