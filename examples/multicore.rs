// Example for the Raspberry Pi Pico. Other HALs will probably implement things differently, and
// other devices might have different hardware, so this is just orientative.
//
// The crate should be compatible with any device who's HAL implements OutputPin from embedded_hal

#![no_std]
#![no_main]

// Raspberry pi pico stuff
use bsp::entry;
use defmt_rtt as _;
use panic_probe as _;
use rp_pico as bsp;
use bsp::hal::{
    sio::Sio,
    gpio::Pins,
    multicore::{Multicore, Stack},
    pac::{CorePeripherals, Peripherals},
};
const XTAL_FREQ_HZ: u32 = 12_000_000u32;

// To read the pusbutton
use embedded_hal::digital::v2::InputPin;

use sevseg_3642bs::Display;

// Multicore stuff
static mut CORE1_STACK: Stack<4096> = Stack::new();
const CORE1_TASK_COMPLETE: u32 = 0xEE;

// Code that will run on core 1. Reads the value being passed from core 0 and displays it.
fn core1_task() -> ! {
    // Raspberry pi pico stuff
    let mut pac = unsafe { Peripherals::steal() };
    let core = unsafe { CorePeripherals::steal() };
    let mut sio = Sio::new(pac.SIO);
    let pins = Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // Create the delay struct required by the display.
    // The display struct will take ownership of the delay struct, since bloacking delays
    // shouldn't be used while using the display.
    let delay = cortex_m::delay::Delay::new(core.SYST, XTAL_FREQ_HZ);

    // Define which pins you used for the 7 segment display. Any GPIO pins can be used.
    // Also pass the delay struct.
    let mut display = Display::new(
        pins.gpio16.into_push_pull_output(),    // A segment
        pins.gpio17.into_push_pull_output(),    // B segment
        pins.gpio18.into_push_pull_output(),    // C sengent
        pins.gpio19.into_push_pull_output(),    // D segment
        pins.gpio20.into_push_pull_output(),    // E segment
        pins.gpio21.into_push_pull_output(),    // F segment
        pins.gpio22.into_push_pull_output(),    // G segment
        pins.gpio26.into_push_pull_output(),    // Time dots segment
        pins.gpio15.into_push_pull_output(),    // First common anode
        pins.gpio14.into_push_pull_output(),    // Second common anode
        pins.gpio13.into_push_pull_output(),    // Third common anode
        pins.gpio12.into_push_pull_output(),    // Fourth common anode
        delay,
    );

    // Clear the display for good measure. This isn't really necessary though.
    display.clear().unwrap();

    loop {
        let input = sio.fifo.read();
        let value = match input {
            Some(val) => val,
            None => 0,
        };

        display.hex(value as i32).unwrap();
        sio.fifo.write_blocking(CORE1_TASK_COMPLETE);
    }
}

#[entry]
fn main() -> ! {
    // Raspberry pi pico stuff
    let mut pac = Peripherals::take().unwrap();
    let mut sio = Sio::new(pac.SIO);
    let pins = Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );
    
    // The push button is connedcted to gpio pin 27
    let readinput = pins.gpio27.into_pull_up_input();
    
    // Multicore stuff
    let mut mc = Multicore::new(&mut pac.PSM, &mut pac.PPB, &mut sio.fifo);
    let cores = mc.cores();
    let core1 = &mut cores[1];
    let _test = core1.spawn(unsafe { &mut CORE1_STACK.mem }, move || {
        core1_task()
    });

    // Used to know if there has been a state change
    let mut prev_btn_status = readinput.is_low().unwrap();
    let mut count = 0;

    loop {
        let btn_status = readinput.is_low().unwrap();

        // If the button has been pressed, increment the count by one
        if btn_status == false && btn_status != prev_btn_status {
            count += 1;
        }
        
        // Send core 1 the count so it can be displayed
        sio.fifo.write(count);

        // Make sure core 1 is working as expected before continuing
        let _ = sio.fifo.read_blocking();

        // Update the previous button status in order to be able to detect a state change on the next loop
        prev_btn_status = btn_status;
    }
}
